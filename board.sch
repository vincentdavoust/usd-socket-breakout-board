EESchema Schematic File Version 2  date Tue Dec 17 19:18:43 2013
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "17 dec 2013"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 7600 2000
NoConn ~ 7600 2700
Text GLabel 6550 2450 2    60   UnSpc ~ 0
GND
Text GLabel 6550 2350 2    60   UnSpc ~ 0
VCC
Text GLabel 7600 2400 0    60   UnSpc ~ 0
VCC
Text GLabel 7600 2200 0    60   UnSpc ~ 0
GND
Text GLabel 6550 1950 2    60   UnSpc ~ 0
CS
Text GLabel 6550 2050 2    60   UnSpc ~ 0
CLK
Text GLabel 6550 2150 2    60   UnSpc ~ 0
MOSI
Text GLabel 6550 2250 2    60   UnSpc ~ 0
MISO
Text GLabel 7600 2600 0    60   UnSpc ~ 0
CS
Text GLabel 7600 2500 0    60   UnSpc ~ 0
MOSI
Text GLabel 7600 2300 0    60   UnSpc ~ 0
CLK
Text GLabel 7600 2100 0    60   UnSpc ~ 0
MISO
$Comp
L CONN_8 P1
U 1 1 52B09166
P 6200 2100
F 0 "P1" V 6150 2100 60  0000 C CNN
F 1 "CONN_8" V 6250 2100 60  0000 C CNN
F 2 "~" H 6200 2100 60  0000 C CNN
F 3 "~" H 6200 2100 60  0000 C CNN
	1    6200 2100
	-1   0    0    1   
$EndComp
Wire Wire Line
	7250 1850 6550 1850
$Comp
L CONN_10 P2
U 1 1 52B09186
P 7950 2250
F 0 "P2" V 7900 2250 60  0000 C CNN
F 1 "CONN_10" V 8000 2250 60  0000 C CNN
F 2 "~" H 7950 2250 60  0000 C CNN
F 3 "~" H 7950 2250 60  0000 C CNN
	1    7950 2250
	1    0    0    1   
$EndComp
Text GLabel 6550 1750 2    60   UnSpc ~ 0
GND
Text GLabel 7600 1900 0    60   UnSpc ~ 0
GND
Wire Wire Line
	7600 1800 7250 1800
Wire Wire Line
	7250 1800 7250 1850
$EndSCHEMATC
